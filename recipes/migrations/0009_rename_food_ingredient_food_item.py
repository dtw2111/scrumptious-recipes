# Generated by Django 4.0.3 on 2022-07-15 21:13

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0008_rename_food_item_ingredient_food'),
    ]

    operations = [
        migrations.RenameField(
            model_name='ingredient',
            old_name='food',
            new_name='food_item',
        ),
    ]
