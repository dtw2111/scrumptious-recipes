# Generated by Django 4.0.3 on 2022-07-15 02:23

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0004_tab'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Tab',
            new_name='Tag',
        ),
    ]
