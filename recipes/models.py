from django.db import models


class Recipe(models.Model):
    name = models.CharField(max_length=125)
    author = models.CharField(max_length=100)
    description = models.TextField()
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name + " by " + self.author


class Measure(models.Model):
    name = models.CharField(max_length=30, unique=True)
    abbreviation = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.name


class FoodItem(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Ingredient(models.Model):
    amount = models.FloatField()
    recipe = models.ForeignKey(
        "Recipe",
        on_delete=models.CASCADE,
        related_name="ingredients",
        null=True,
    )
    measure = models.ForeignKey(
        "Measure",
        on_delete=models.PROTECT,
        related_name="ingredients",
        null=True,
    )
    food = models.ForeignKey(
        "FoodItem",
        on_delete=models.PROTECT,
        related_name="ingredients",
        null=True,
    )

    def __str__(self):
        return f"{self.amount} {self.measure} of {self.food}"


class Step(models.Model):
    recipe = models.ForeignKey(
        "Recipe",
        on_delete=models.CASCADE,
        related_name="steps",
        null=True,
    )
    order = models.PositiveSmallIntegerField()
    directions = models.CharField(max_length=300)
    food_items = models.ManyToManyField(
        "FoodItem", related_name="steps", null=True, blank=True
    )

    def __str__(self):
        return f"Step {self.order} in {self.recipe}"
